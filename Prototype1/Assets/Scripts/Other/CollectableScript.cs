﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableScript : MonoBehaviour 
{
	public float lifeTime;
	public float maxHealth;
	public int rotationSpeed;
	private GameCordGeneral gameCord;
	private MyGameManager gameManager;
	public Arrow arrowContainer;
	Color playerColor;
	Light light;
    public ParticleSystem particle;
	UnityEngine.UI.Text percentText;

    [FMODUnity.EventRef]
    public string batterySound = "event:/Battery";
    FMOD.Studio.EventInstance batteryEvent;

    [FMODUnity.EventRef]
    public string batteryPick = "event:/BatteryPickup";

    // Use this for initialization
    void Start () 
	{
		gameCord = GameObject.FindGameObjectWithTag("GameCoordinator").GetComponent<GameCordGeneral>();
		gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<MyGameManager>();
		percentText = GameObject.Find ("Text_BatteryLabel").GetComponent<UnityEngine.UI.Text> ();

        batteryEvent = FMODUnity.RuntimeManager.CreateInstance(batterySound);
        batteryEvent.start();

        for (int i = 0; i < GameObject.FindGameObjectWithTag("Player").transform.childCount; i++)
		{
			Transform child = GameObject.FindGameObjectWithTag("Player").transform.GetChild (i);
			if (child.name == "ArrowContainer") {
				arrowContainer = child.GetComponent<Arrow> ();
				break;
			} else if (child.name == "polySurface4") {
				playerColor = child.GetComponent<Renderer> ().material.color;
			} else if (child.name == "Main Camera") {
				light = child.Find ("PurpleLight").GetComponent<Light> ();
			}

		}
	}

	// Update is called once per frame
	void Update ()
	{
		#if UNITY_EDITOR
		Debug.DrawRay (transform.position, transform.up * 5f, Color.blue);
		#endif

		if (gameManager.e_State == EGameState.STATE_GAME_STARTED) 
		{
			TickDownToDeath ();
		}
		DoABarrelRoll ();

        batteryEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
    }


	// ticks the lifeTime down till its 0
	// on zero it despawns and informs the game manager 
	void TickDownToDeath()
	{
		if (lifeTime > 0)
		{
			lifeTime -= Time.deltaTime;
			float percent = lifeTime / maxHealth;
			percentText.text = ((int)(percent*100)).ToString () + "%";
			GameObject.FindGameObjectWithTag("Player").transform.Find("polySurface4").GetComponent<Renderer>().material.color = new Color (percent, percent, percent);
			light.intensity = 7.32f * percent;
		}
		else 
		{
            batteryEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
			// if the player doesnt get the collectable in time the player loses the game
			gameManager.lastDeathReason = "Out Of Bateries";
			gameManager.e_State = EGameState.STATE_GAMEOVER;
		}

	}

	// makes the collectable spin 
	void DoABarrelRoll()
	{

		Vector3 rot = new Vector3 (1, 1, 1);
		transform.Rotate (rot, rotationSpeed * Time.deltaTime);
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.CompareTag("Player"))
		{
			//maybe in the future add points

			//inform game cordinator for new respawn
			arrowContainer.collectablePrefab = null;
            particle.Play();
            particle.transform.SetParent(null);
            batteryEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            FMODUnity.RuntimeManager.PlayOneShot(batteryPick);
            Destroy(this.gameObject);
			gameCord.canSpawnColl = true;
		}
        else if (col.gameObject.CompareTag("wall")|| col.gameObject.CompareTag("trap"))
        {
            //maybe in the future add points

            //inform game cordinator for new respawn
            Debug.Log("i found an obstacles so i respawned");
            Destroy(this.gameObject);
            gameCord.canSpawnColl = true;
        }
    }
}
