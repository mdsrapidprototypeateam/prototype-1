﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

    public MyGameManager gameManager;
    public Transform arrowTrf;
	public Transform collectablePrefab;

	private Transform gameCoordManager;

	void Start ()
    {
        if (!gameManager)
            gameManager = (MyGameManager)GameObject.Find("GameManager").GetComponent<MyGameManager>();

		if (!gameCoordManager)
			gameCoordManager = GameObject.FindWithTag("GameCoordinator").transform;

		if (!gameCoordManager)
			Debug.LogError ("No GameCoordinatorManager in scene");

        if (!arrowTrf)
            arrowTrf = transform.GetChild(0).transform;

		if (!arrowTrf)
			Debug.LogError ("No arrow model in scene");
		else
		{
			arrowTrf.gameObject.SetActive (false);
		}
	}
	
	void Update ()
	{
		if (gameManager.e_State == EGameState.STATE_GAME_STARTED)
		{
			if (arrowTrf)
			{
				if (!arrowTrf.gameObject.activeSelf)
					arrowTrf.gameObject.SetActive (true);

				Vector3 collectablePosition;
				Vector3 newOrientationVector;

				if (!collectablePrefab && gameCoordManager.childCount > 0)
				{
					collectablePrefab = gameCoordManager.GetChild (0);
				}

				if (collectablePrefab)
				{
					collectablePosition = collectablePrefab.transform.position;
					newOrientationVector = ((collectablePosition + new Vector3 (0, transform.position.y, 0)) - transform.position).normalized;

#if UNITY_EDITOR
					Debug.DrawRay (transform.position, newOrientationVector * 3f, Color.red);
#endif
					arrowTrf.forward = newOrientationVector * -1;
				}
			}
		}

		else if (gameManager.e_State == EGameState.STATE_GAMEOVER)
		{
			if (arrowTrf)
			{
				if (arrowTrf.gameObject.activeSelf)
					arrowTrf.gameObject.SetActive (false);
			}
		}
	}
}
