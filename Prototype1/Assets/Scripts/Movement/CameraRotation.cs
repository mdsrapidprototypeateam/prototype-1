﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {

	public MyGameManager gameManager;
	public GameObject ragdoll;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (gameManager.e_State == EGameState.STATE_GAMEOVER)
		{
			transform.RotateAround(ragdoll.transform.position, Vector3.up, 0.5f);
		}
	}
}
