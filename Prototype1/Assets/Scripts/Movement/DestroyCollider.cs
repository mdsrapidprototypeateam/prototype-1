﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine("Kill");
        StartCoroutine("Activate");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator Activate()
    {
        yield return new WaitForSeconds(0.5f);
        tag = "trap";
    }

    IEnumerator Kill()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }
}
