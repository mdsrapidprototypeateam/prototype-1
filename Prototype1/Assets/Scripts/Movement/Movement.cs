﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using XInputDotNetPure;

public class Movement : MonoBehaviour {

    public MyGameManager gameManager;

    bool playerIndexSet = false;
	PlayerIndex playerIndex;
	GamePadState state;
	GamePadState prevState;

    public float yRotation = 0f;
    public float yRotationSpeed = 10f;
    public float characterSpeed = 10f;

    public Vector2 inputDirection;

    public float slowdownMultiplier;
    private float timeMult = 1f;
    public float slowDuration;
    public float slowCooldown;

    public TrailRenderer line;

	public Camera cam;
	public float camMoveSpeed;

	public GameObject ragdoll;

    public UnityEngine.PostProcessing.PostProcessingProfile standardProf;
    public UnityEngine.PostProcessing.PostProcessingProfile slowProf;
    UnityEngine.PostProcessing.PostProcessingBehaviour postProcessing;

    public bool shouldStartRunning;
    bool canSlow = true;

	Transform cooldown;

    [FMODUnity.EventRef]
    public string rolling = "event:/Walking";       //Create the eventref and define the event path
    FMOD.Studio.EventInstance walkingEvent;

    [FMODUnity.EventRef]
    public string slowing = "event:/Slow";

    [FMODUnity.EventRef]
    public string slowReverse = "event:/SlowReverse";

    void Start()
	{
        if ( !gameManager )
        {
            gameManager = (MyGameManager) GameObject.Find("GameManager").GetComponent<MyGameManager>();
        }

        postProcessing = cam.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>();
		cooldown = GameObject.Find ("CooldownFBX").transform;
    }
		
	void Update()
	{
		// Find a PlayerIndex, for a single player game
		// Will find the first controller that is connected ans use it
		if (!playerIndexSet || !prevState.IsConnected)
		{
			for (int i = 0; i < 4; ++i)
			{
				PlayerIndex testPlayerIndex = (PlayerIndex)i;
				GamePadState testState = GamePad.GetState(testPlayerIndex);
				if (testState.IsConnected)
				{
					Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
					playerIndex = testPlayerIndex;
					playerIndexSet = true;
                    break;
				}
			}
		}

		prevState = state;
		state = GamePad.GetState(playerIndex);

        if (playerIndexSet || state.IsConnected)
        {
            //inputDirection = new Vector2(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y);
			inputDirection = new Vector2(state.ThumbSticks.Left.X, state.Triggers.Right);
        } else
        {
            inputDirection = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        }

        if ( gameManager.e_State == EGameState.STATE_IDLE )
        {
            if ( inputDirection.y != 0)
                gameManager.e_State = EGameState.STATE_COUNTDOWN;
        }

		else if ( gameManager.e_State == EGameState.STATE_COUNTDOWN )
		{
			if (inputDirection.y > 0.7f)
				gameManager.isPressingControls = true;
			else
				gameManager.isPressingControls = false;
		}

        else if ( gameManager.e_State == EGameState.STATE_GAME_STARTED )
        {
            if (shouldStartRunning)
            {
                walkingEvent = FMODUnity.RuntimeManager.CreateInstance(rolling);
                walkingEvent.start();
                shouldStartRunning = false;
                Debug.Log("Started Sound");
            }

			if (inputDirection.y == 0)
			{
				gameManager.lastDeathReason = "Stopped Moving";
				gameManager.e_State = EGameState.STATE_GAMEOVER;
			}
            //Invoke("KillPlayerByNotMoving", 0.35f);

            if ( (state.Buttons.A == ButtonState.Pressed && prevState.Buttons.A == ButtonState.Released) || Input.GetButtonDown("Fire1"))
            {
                if (canSlow)
                {
                    if (timeMult != slowdownMultiplier)
                    {
                        Time.timeScale /= slowdownMultiplier;
                        timeMult = slowdownMultiplier;
                        GetComponent<Animator>().speed *= slowdownMultiplier;
                        canSlow = false;
                        //line.time /= slowdownMultiplier;
                        postProcessing.profile = slowProf;
                        FMODUnity.RuntimeManager.PlayOneShot(slowing);
                        StartCoroutine (CameraMovement (75f));
                        StartCoroutine(SlowDuration());
                        
                    }
                }
            }

            else
            {
                //if ( IsInvoking("KillPlayerByNotMoving") )
                    //CancelInvoke("KillPlayerByNotMoving");
                yRotation += inputDirection.x * yRotationSpeed * Time.deltaTime * timeMult;
                transform.eulerAngles = new Vector3(0, yRotation, 0);
                transform.position += (transform.forward * inputDirection.y).normalized * characterSpeed * Time.deltaTime * timeMult;
            }
        }
        else if (gameManager.e_State == EGameState.STATE_GAMEOVER)
        {
			ragdoll.transform.position = transform.position;
			ragdoll.SetActive (true);
			ragdoll.transform.GetChild (1).GetChild (0).GetComponent<Rigidbody> ().AddForce (-transform.forward * 100, ForceMode.Impulse);
			cam.transform.parent = null;

            walkingEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            walkingEvent.release();
            cam.fieldOfView = 100f;
			//cam.transform.RotateAround(ragdoll.transform.position, Vector3.up, 0.5f);
			cam.GetComponent<CameraRotation>().ragdoll = ragdoll;
			gameObject.SetActive (false);
        }
        else
        {
            walkingEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            walkingEvent.release();
        }

       
    }

    private void KillPlayerByNotMoving()
    {
		gameManager.lastDeathReason = "Stopped Moving";
        gameManager.e_State = EGameState.STATE_GAMEOVER;
    }

    IEnumerator SlowDuration()
    {
		for (int i = 0; i < 5; i++) {
			cooldown.GetChild (i).gameObject.SetActive (false);
		}
        gameManager.musicEvent.setParameterValue("Slowed", 1f);
        yield return new WaitForSecondsRealtime(slowDuration);
        Time.timeScale = 1;
        timeMult = 1f;
        GetComponent<Animator>().speed /= slowdownMultiplier;
        gameManager.musicEvent.setParameterValue("Slowed", 0f);
		line.time /= slowdownMultiplier;
        line.time *= slowdownMultiplier;
		StartCoroutine (CameraMovement (60f));
        postProcessing.profile = standardProf;
        FMODUnity.RuntimeManager.PlayOneShot(slowReverse);
		StartCoroutine(SlowCooldown());
    }

    IEnumerator SlowCooldown()
    {
		for (int i = 0; i < 5; i++) {
			
			yield return new WaitForSecondsRealtime (slowCooldown / 5f);
			cooldown.GetChild (4-i).gameObject.SetActive (true);

		}
        canSlow = true;
    }

	IEnumerator CameraMovement(float targetFov)
	{
        
		float time = 0f;
		while (cam.fieldOfView != targetFov) {
			cam.fieldOfView = Mathf.Lerp (cam.fieldOfView, targetFov, time);
			time += Time.unscaledDeltaTime * camMoveSpeed;
			yield return new WaitForEndOfFrame ();
		}

		yield return null;
	}
}
