﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnColliders : MonoBehaviour {

    public float timeToSpawn;
    float timeElapsed;
    public GameObject col;
    bool gameStarted;
    public MyGameManager manager;

	// Use this for initialization
	void Start ()
    {
        if (!manager)
        {
            manager = (MyGameManager)GameObject.Find("GameManager").GetComponent<MyGameManager>();
        }

    }
	
	// Update is called once per frame
	void Update () {

        if (gameStarted)
        {

            if (timeElapsed > timeToSpawn)
            {
				Instantiate(col, transform.position, Quaternion.Euler(0, transform.parent.GetComponent<Movement>().yRotation, 0));
                timeElapsed = 0;
            }

            timeElapsed += Time.deltaTime;
        } else
        {
            if (manager.e_State == EGameState.STATE_GAME_STARTED)
            {
                gameStarted = true;
                timeElapsed = timeToSpawn;
            }
        }
	}
}
