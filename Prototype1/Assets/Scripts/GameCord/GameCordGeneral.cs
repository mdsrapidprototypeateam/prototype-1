﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class GameCordGeneral : MonoBehaviour
{
	public GameObject[] objToSpawn;
	public GameObject collectable;

	public GameObject generator;
	private MyGameManager gameManager;
    private int[][] baseMap;
    private int gridSizeX;
    private int gridSizeY;
	[HideInInspector]
	public bool canSpawnColl;

	public float timeToSpawnColl;

	[HideInInspector]
	public List<GameObject> floorTiles;

	public List<GameObject>activeTraps;
	public List<WaveBase>nextWaves;
	public List<WaveBase> OldWaves;
	public WaveBase resetWave;



	bool playerIndexSet = false;
	PlayerIndex playerIndex;
	GamePadState state;
	GamePadState prevState;


	// Use this for initialization
	void Start ()
    {
		generator = GameObject.FindGameObjectWithTag ("Generator");
		gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<MyGameManager>();
		FindFloorTiles ();
		SpawnTraps (true);

		for (int i = 0; i < activeTraps.Count; i++) 
		{
			resetWave.addToWorld.Add (activeTraps [i]);
		}

	}
	
	// Update is called once per frame
	void Update ()
    {
		if (!playerIndexSet || !prevState.IsConnected)
		{
			for (int i = 0; i < 4; ++i)
			{
				PlayerIndex testPlayerIndex = (PlayerIndex)i;
				GamePadState testState = GamePad.GetState(testPlayerIndex);
				if (testState.IsConnected)
				{
					Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
					playerIndex = testPlayerIndex;
					playerIndexSet = true;
                    break;
				}
			}
		}

		prevState = state;
		state = GamePad.GetState(playerIndex);

		UpdateCollSpawn ();
		SpawnCollectable ();
		CheckNextWave ();

		//cheat codes
		if (Input.GetKeyDown (KeyCode.L)||state.Buttons.Y == ButtonState.Pressed && prevState.Buttons.Y == ButtonState.Released) 
		{
			if (nextWaves.Count != 0) 
			{
				gameManager.score = nextWaves [0].scoreTrigger * 10;
			}
			else 
			{

				for (int i = 0; i < activeTraps.Count; i++) 
				{
					resetWave.removeFromWorld.Add (activeTraps [i]);
				}
				for (int i = 0; i < OldWaves.Count; i++) 
				{
					nextWaves.Add (OldWaves [i]);
				}

				GenerateTraps (resetWave);
				SpawnTraps (false);

				OldWaves.Clear ();
				gameManager.score = 0;

			}

		}
	}


    // set parameters for the base grid map
    void GenerateBaseMap()
    {
        // grid size
        gridSizeX = 50;
        gridSizeY = 50;
		  
    }


	// fetchs all the childs from the grid generator and stores them in an array for easy access\
	// this is used for now to spawn the collectable item
	// in the future should be used to spawn all entities
	void FindFloorTiles()
	{
		for (int i = 0; i < generator.transform.childCount; i++)
		{
			GameObject child = generator.transform.GetChild (i).gameObject;
			//Debug.Log (child.transform.position);
			floorTiles.Add (child);
		}


		/*foreach (GameObject child in generator.transform)
		{
			Debug.Log (child.transform.position);
			floorTiles.Add (child);
		}*/

	}


	//Evalutates if a random tile is available to have the collectable
	//Then spawns the objcte with a specific timer based on the current score of the player
	//The higher the score the less time is available for picking up the collectable
	public void SpawnCollectable()
	{
		if (canSpawnColl) 
		{
			int gridsize = generator.GetComponent<gridgenerator> ().gridX * generator.GetComponent<gridgenerator> ().gridY;
			int randomTile = Mathf.RoundToInt (Random.Range (0, gridsize - 1));
			//check is the tile is ocupied by other ojects
			if (floorTiles [randomTile].GetComponent<gridTile> ().isTaken == true) 
			{
				// it is ocupied to run the random again
				SpawnCollectable ();
			} 
			else 
			{
				//its not ocupied sapwn the collectable
				Vector3 spawnPos = floorTiles [randomTile].transform.position;
				Vector3 deviation = new Vector3 (-1, 1, -1);
				spawnPos += deviation;
				GameObject collectableSpawn = Instantiate (collectable, spawnPos, Quaternion.identity, this.transform);
				//check the current score
				float score = gameManager.score;
				if (score < 30000) {
					collectableSpawn.GetComponent<CollectableScript> ().lifeTime = 100;
					collectableSpawn.GetComponent<CollectableScript> ().maxHealth = 100;
				} else if (score >= 30000 && score < 60000) {
					collectableSpawn.GetComponent<CollectableScript> ().lifeTime = 60;
					collectableSpawn.GetComponent<CollectableScript> ().maxHealth = 60;
				} else if (score >= 60000 && score < 120000) {
					collectableSpawn.GetComponent<CollectableScript> ().lifeTime = 40;
					collectableSpawn.GetComponent<CollectableScript> ().maxHealth = 40;
				} else if (score >= 120000) {
					collectableSpawn.GetComponent<CollectableScript> ().lifeTime = 30;
					collectableSpawn.GetComponent<CollectableScript> ().maxHealth = 30;
				}
			}
			canSpawnColl = false;
		}
	}

	// responsable for waiting for all the walls to spawn to allow colectables to spawn
	void UpdateCollSpawn()
	{
		if (timeToSpawnColl > 0)
		{
			timeToSpawnColl -= Time.deltaTime;

		}
		else if(timeToSpawnColl<=0 && timeToSpawnColl>-10)
		{
			canSpawnColl=true;
			timeToSpawnColl = -10000;
		}
	}



	//check the waves in the nextWaves list for their score triggers in order to activate them
	//and then removes them
	void CheckNextWave()
	{
		for (int i = 0; i < nextWaves.Count; i++) 
		{
			if (gameManager.score / 10 >= nextWaves [i].scoreTrigger)
			{
				GenerateTraps (nextWaves [i]);
				SpawnTraps (false);
				OldWaves.Add(nextWaves [i]);
				nextWaves.Remove (nextWaves [i]);
			}

		}

	}


	void GenerateTraps(WaveBase nextWave)
	{
		for (int j = 0; j < nextWave.removeFromWorld.Count; j++)
		{
			if (activeTraps.Contains (nextWave.removeFromWorld [j])) 
			{

				nextWave.removeFromWorld [j].GetComponent<TrapSetup> ().SetInactive ();
				activeTraps.Remove (nextWave.removeFromWorld [j]);
			}
		}

		for (int k = 0; k < nextWave.addToWorld.Count; k++) 
		{
			activeTraps.Add (nextWave.addToWorld [k]);

		}
	}



	void SpawnTraps(bool start)
	{
		if (start)
		{
			for (int i = 0; i < activeTraps.Count; i++)
			{
				activeTraps [i].gameObject.SetActive (true);
			}

		} 
		else 
		{
			for (int i = 0; i < activeTraps.Count; i++) 
			{
				if (!activeTraps [i].activeInHierarchy) 
				{
					activeTraps [i].gameObject.SetActive (true);

				}

			}

		}



	}

}
