﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapSetup : MonoBehaviour
{
	public GameObject warning;
	public GameObject trap;
	public float warningTimer;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void SetInactive()
	{
		trap.SetActive (false);
		warning.SetActive (false);
		this.gameObject.SetActive (false);

	}

	public void WarningTimerDone()
	{
		trap.SetActive (true);
	}

	void OnEnable()
	{
		trap.SetActive (false);
		warning.SetActive (true);
		warning.GetComponent<WarningTimer> ().wTimer = warningTimer;
	}
}

