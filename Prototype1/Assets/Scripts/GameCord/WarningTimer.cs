﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningTimer : MonoBehaviour 
{
	[HideInInspector]
	public float wTimer;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (wTimer >= 0) 
		{
			wTimer -= Time.deltaTime;
		} 
		else 
		{
			gameObject.GetComponentInParent<TrapSetup> ().WarningTimerDone ();
			this.gameObject.SetActive (false);
		}
	}
}
