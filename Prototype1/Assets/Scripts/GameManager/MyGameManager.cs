﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum EGameState
{
    //NONE,
    STATE_IDLE,
    STATE_COUNTDOWN,
    STATE_GAME_STARTED,
    STATE_GAMEOVER
    //STATE_WIN
}

public class MyGameManager : MonoBehaviour
{

    public EGameState e_State = EGameState.STATE_IDLE;
    private EGameState m_EState;

    public int countdownTimer;
    public bool updateCountdownTimer = false;
    public bool finishCountdownTimer = false;
    public float intervalToDisplayRun = 3.0f;
    public float intervalToRestartGame = 3.0f;
    public float intervalToUpdateCounter = 1.0f;
    public bool isPressingControls = false;
    public float score = 0;
    public string lastDeathReason = "";

    public GameObject countdownObj;
    public GameObject scoreCountObj;

    public Animator playerAnimator;

    public delegate void EGameStateChanged(EGameState value);
    public event EGameStateChanged OnEGameStateChangedEvent;
    public XmlDbManager dbManager;

    public Text[] scoreValues;
    public Text scorelabels;
    public Text holdAnalogLabel;


    [FMODUnity.EventRef]
    public string music = "event:/MainTheme";       //Create the eventref and define the event path
    public FMOD.Studio.EventInstance musicEvent;
    FMOD.Studio.PLAYBACK_STATE playbackState;
    bool musicStarted = false;

    public static MyGameManager GetInstance()
    {
        return GameObject.FindGameObjectWithTag("GameManager").GetComponent<MyGameManager>();
    }

    void Start()
    {
        Debug.Log("Manager start");

        StartEGameStateCounter();

        if (!e_State.Equals(EGameState.STATE_IDLE))
            e_State = EGameState.STATE_IDLE;

        scoreCountObj.SetActive(false);

        playerAnimator = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();

        //Debug.Log(musicStarted);
        FMODUnity.RuntimeManager.GetBus("bus:/").stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
        //if(!musicPaused)
        //{
        musicEvent = FMODUnity.RuntimeManager.CreateInstance(music);
        musicEvent.start();
        
        musicStarted = true;
        //}
        //Debug.Log(musicStarted);

        //dbManager = GetComponent<XmlDbManager>();
    }

    void Update()
    {

        //UpdateScoreTable();

        GetEGameState();
        if (SceneManager.GetActiveScene().name.Contains("Menu"))
        {

        }
        else
        {
            if (e_State == EGameState.STATE_COUNTDOWN && finishCountdownTimer)
            {
                CancelInvoke("IncrementCountdownCounter");
                e_State = EGameState.STATE_GAME_STARTED;
            }
        }
    }



    private void StartEGameStateCounter()
    {
        OnEGameStateChangedEvent += OnEGameStateChanged;
    }

    public void GetEGameState()
    {
        if (e_State != m_EState)
        {
            m_EState = e_State;

            if (OnEGameStateChangedEvent != null)
                OnEGameStateChangedEvent(m_EState);
        }
    }

    private void OnEGameStateChanged(EGameState value)
    {
        // state effects

        switch (value)
        {
            case EGameState.STATE_IDLE:
            default:
                Debug.Log("Waiting player input for start");
                playerAnimator.enabled = false;
                //dbManager.LoadScoreFromXML(dbManager.scoreDbXmlFile);
                /*foreach (Text _text in scoreValues)
                {
                    _text.gameObject.SetActive(true);
                }
                scorelabels.gameObject.SetActive(true);*/
                break;

            case EGameState.STATE_COUNTDOWN:
                Debug.Log("Start Countdown");
                finishCountdownTimer = false;
                /*foreach (Text _text in scoreValues)
                {
                    _text.gameObject.SetActive(false);
                }
                scorelabels.gameObject.SetActive(false);*/
                InvokeRepeating("IncrementCountdownCounter", 1f, intervalToUpdateCounter);
                break;

			case EGameState.STATE_GAME_STARTED:
				Debug.Log ("Game started");
				playerAnimator.SetTrigger ("Started");
                GameObject.FindGameObjectWithTag("Player").GetComponent<Movement>().shouldStartRunning = true;
                Invoke("ToggleHUDObjects", intervalToDisplayRun);
                InvokeRepeating("IncrementScore", 0.1f, 0.1f);
                score = 0;
                playerAnimator.enabled = true;
                if (!scoreCountObj.activeSelf)
                {
                    scoreCountObj.SetActive(true);
                }
                holdAnalogLabel.gameObject.SetActive(false);
                break;

            case EGameState.STATE_GAMEOVER:
                Debug.Log("Player died");
                Time.timeScale = 1;
                playerAnimator.speed = 1;
                countdownObj.SetActive(true);


                /*foreach (Text _text in scoreValues)
                {
                    _text.gameObject.SetActive(true);
                }
                scorelabels.gameObject.SetActive(true);*/
                UpdateScore();
                /*if (!scoreCountObj.activeSelf)
                {
                    scoreCountObj.SetActive(true);
                    CancelInvoke("ToggleHUDObjects");
                }*/
                CancelInvoke();
                Invoke("ChangeFromGameoverBackToIdle", intervalToRestartGame);
                playerAnimator.enabled = false;
                break;
        }
    }

    private void IncrementCountdownCounter()
    {
        if (isPressingControls)
            updateCountdownTimer = true;
    }

    private void ToggleHUDObjects()
    {
        if (countdownObj.activeSelf)
            countdownObj.SetActive(false);


        /*else if ( !scoreCountObj.activeSelf )
        {
            scoreCountObj.SetActive(true);
            CancelInvoke("ToggleHUDObjects");
        }*/
    }

    private void IncrementScore()
    {
        score += 100;
    }

    private void ChangeFromGameoverBackToIdle()
    {
        ApplicationManager.GetInstance().DisplayInGameMenu();


        //e_State = EGameState.STATE_COUNTDOWN;

        //musicEvent.stop();

        //musicEvent.release();
        //FMODUnity.RuntimeManager.GetBus("bus:/").stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
        //FMODUnity.RuntimeManager.GetBus()

        //int scene = SceneManager.GetActiveScene().buildIndex;
        //SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

    private void UpdateScore()
    {
        ApplicationManager.UpdatePlayerLastScore(Mathf.RoundToInt(score)/10);
        ApplicationManager.UpdatePlayerPlayCount(1);

        switch (lastDeathReason)
        {
            case "Stopped Moving":
                ApplicationManager.UpdatePlayerLastDeathReason(DatabaseCalls.NdstoppedName);
                break;
            case "Burnt by Laser":
                ApplicationManager.UpdatePlayerLastDeathReason(DatabaseCalls.NdlaserName);
                break;
            case "Impaled by Spike Trap":
                ApplicationManager.UpdatePlayerLastDeathReason(DatabaseCalls.NdspykesName);
                break;
            case "Crashed against wall":
                ApplicationManager.UpdatePlayerLastDeathReason(DatabaseCalls.NdwallName);
                break;
            case "Out Off batteries":
                ApplicationManager.UpdatePlayerLastDeathReason(DatabaseCalls.NddepletedName);
                break;
        }

        ApplicationManager.UpdatePlayerLastDeathReasonCountn(1);

        //         XmlDbManager.UpdateScoreProperty(dbManager.numberOfGames, dbManager.highestScore,
        //             dbManager.numberDeathsByWall, dbManager.numberDeathsBySpykes, dbManager.numberDeathsByLaser,
        //             dbManager.numberDeathsByStopping, dbManager.numberDeathsByRunningOutOffBateries, dbManager.scoreDbXmlFile);
    }

    private void UpdateScoreTable()
    {
        scoreValues[0].text = dbManager.highestScore.ToString() + "\n\n\n\n\n\n\n\n\n\n";
        scoreValues[1].text = dbManager.numberOfGames.ToString() + "\n\n\n\n\n\n\n\n\n";

        scoreValues[2].text = dbManager.numberDeathsByStopping.ToString() + "\n\n\n\n\n";
        scoreValues[3].text = dbManager.numberDeathsByLaser.ToString() + "\n\n\n\n";
        scoreValues[4].text = dbManager.numberDeathsBySpykes.ToString() + "\n\n\n";
        scoreValues[5].text = dbManager.numberDeathsByWall.ToString() + "\n\n";
        scoreValues[6].text = dbManager.numberDeathsByRunningOutOffBateries.ToString() + "\n";
    }
}
