﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    public Text m_MyText;
    public MyGameManager gameManager;
    public int i_CountdownTimer = 3;

    void Start()
    {
        if ( !gameManager )
        {
            gameManager = (MyGameManager) GameObject.Find("GameManager").GetComponent<MyGameManager>();
        }

        if ( m_MyText )
        {
            m_MyText.text = "W a i t i n g\nf o r   i n p u t . . . ";
        }
    }

    void Update()
    {
        if ( m_MyText && gameManager.e_State == EGameState.STATE_COUNTDOWN )
        {
            m_MyText.text = (i_CountdownTimer <= 0) ? "R U N" : i_CountdownTimer.ToString();

            if ( gameManager.updateCountdownTimer )
            {
                gameManager.updateCountdownTimer = false;
                --i_CountdownTimer;

                if ( i_CountdownTimer <= 0 )
                    gameManager.finishCountdownTimer = true;
            }
        }

        else if ( m_MyText && gameManager.e_State == EGameState.STATE_GAME_STARTED && gameManager.finishCountdownTimer )
        {
            m_MyText.text = "R U N";
        }

        else if (m_MyText && m_MyText.gameObject.activeSelf && gameManager.e_State == EGameState.STATE_GAMEOVER && gameManager.finishCountdownTimer)
        {
			m_MyText.text = "Y O U   D I E D !\n" + gameManager.lastDeathReason;
        }
    }
}
