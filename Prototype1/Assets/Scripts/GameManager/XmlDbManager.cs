﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

public class XmlDbManager : MonoBehaviour
{
    public TextAsset scoreDbXmlFile;
    public int numberOfGames = 0;
    public int numberDeathsByWall = 0;
    public int numberDeathsBySpykes = 0;
    public int numberDeathsByLaser = 0;
	public int numberDeathsByStopping = 0;
	public int numberDeathsByRunningOutOffBateries = 0;
    public int highestScore = 0;

    public List<List<Vector3>> zoneCollection = new List<List<Vector3>>();
    //public List<Vector3> firstZone = new List<Vector3>();

    private XmlDocument m_XmlDoc;
    private XmlNodeList m_Scores;

    // Use this for initialization
    void Start()
    {
        //LoadScoreFromXML(scoreDbXmlFile);
    }

    private void LoadXmlFile(TextAsset file)
    {
        m_XmlDoc = new XmlDocument();   // xmlDoc is the new xml document to be loaded
        m_XmlDoc.LoadXml(file.text); // we want to load this file
    }

    public void LoadScoreFromXML(TextAsset file)
    {
        //LoadXmlFile(scoreDbXmlFile); // we load the contents of our file

        string filepath = Application.dataPath + @"/Data/" + file.name + ".xml";
        XmlDocument xmlDoc = new XmlDocument();

        if ( File.Exists(filepath) )
        {
            xmlDoc.Load(filepath);

            // and search for score entries
            m_Scores = xmlDoc.GetElementsByTagName("Score");   // array of scores

            //Debug.Log(m_Scores);
            foreach ( XmlNode score in m_Scores )
            {
                XmlNodeList scoreContentChildren = score.ChildNodes;

                foreach ( XmlNode info in scoreContentChildren )
                {
                    if(info.Name == "numberOfGames" )
                    {
                        numberOfGames = int.Parse(info.InnerText);
                    }
                    if ( info.Name == "numberDeathsByWall" )
                    {
                        numberDeathsByWall = int.Parse(info.InnerText);
                    }
                    if ( info.Name == "numberDeathsBySpykes" )
                    {
                        numberDeathsBySpykes = int.Parse(info.InnerText);
                    }
                    if ( info.Name == "numberDeathsByLaser" )
                    {
                        numberDeathsByLaser = int.Parse(info.InnerText);
                    }
                    if ( info.Name == "numberDeathsByStopping" )
                    {
						numberDeathsByStopping = int.Parse(info.InnerText);
                    }

					if ( info.Name == "numberDeathsByRunningOutOffBateries" )
					{
						numberDeathsByRunningOutOffBateries = int.Parse(info.InnerText); //numberDeathsByRunningOutOffBateries
					}

                    if ( info.Name == "highestScore" )
                    {
                        highestScore = int.Parse(info.InnerText);
                    }
                }
            }
        }
    }

    public static void UpdateScoreProperty(int _numberOfGames, int _highestScore, int _numberDeathsByWall, int _numberDeathsBySpykes, int _numberDeathsByLaser, int _numberDeathsByStopping,
	                                       int _numberDeathsByRunningOutOffBateries, TextAsset file)
    {
        string filepath = Application.dataPath + @"/Data/" + file.name + ".xml";
        XmlDocument xmlDoc = new XmlDocument();

        Debug.Log(filepath);

        if ( File.Exists(filepath) )
        {
            xmlDoc.Load(filepath);

            XmlElement elmRoot = xmlDoc.DocumentElement;
            elmRoot.RemoveAll(); // remove all inside the transforms node.

            XmlElement elmNew = xmlDoc.CreateElement("Score"); // create the Score node.

            XmlElement numberOfGames = xmlDoc.CreateElement("numberOfGames");
            numberOfGames.InnerXml = _numberOfGames.ToString();

            XmlElement numberDeathsByWall = xmlDoc.CreateElement("numberDeathsByWall");
            numberDeathsByWall.InnerXml = _numberDeathsByWall.ToString();

            XmlElement numberDeathsBySpykes = xmlDoc.CreateElement("numberDeathsBySpykes");
            numberDeathsBySpykes.InnerXml = _numberDeathsBySpykes.ToString();

            XmlElement numberDeathsByLaser = xmlDoc.CreateElement("numberDeathsByLaser");
            numberDeathsByLaser.InnerXml = _numberDeathsByLaser.ToString();

            XmlElement numberDeathsByStopping = xmlDoc.CreateElement("numberDeathsByStopping");
            numberDeathsByStopping.InnerXml = _numberDeathsByStopping.ToString();

			XmlElement numberDeathsByRunningOutOffBateries = xmlDoc.CreateElement("numberDeathsByRunningOutOffBateries");
			numberDeathsByRunningOutOffBateries.InnerXml = _numberDeathsByRunningOutOffBateries.ToString();

            XmlElement highestScore = xmlDoc.CreateElement("highestScore");
            highestScore.InnerXml = _highestScore.ToString();

            Debug.Log(highestScore.InnerXml);

            elmNew.AppendChild(numberOfGames); // make the numberOfGames node the parent.
            elmNew.AppendChild(numberDeathsByWall); // make the numberDeathsByWall node the parent.
            elmNew.AppendChild(numberDeathsBySpykes); // make the numberDeathsBySpykes node the parent.
            elmNew.AppendChild(numberDeathsByLaser); // make the numberDeathsByLaser node the parent.
			elmNew.AppendChild(numberDeathsByStopping); // make the numberDeathsByStopping node the parent.
			elmNew.AppendChild(numberDeathsByRunningOutOffBateries); // make the numberDeathsByRunningOutOffBateries node the parent.
            elmNew.AppendChild(highestScore); // make the highestScore node the parent.
            elmRoot.AppendChild(elmNew); // make the transform node the parent.

            xmlDoc.Save(filepath);
        }
    }
}
