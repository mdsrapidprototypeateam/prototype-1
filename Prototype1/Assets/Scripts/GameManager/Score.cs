﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public Text m_MyText;
    public MyGameManager gameManager;


    void Start()
    {
        if (!gameManager)
        {
            gameManager = (MyGameManager)GameObject.Find("GameManager").GetComponent<MyGameManager>();
        }
    }

    void Update()
    {
        if (m_MyText && gameManager.e_State == EGameState.STATE_GAME_STARTED)
        {
            m_MyText.text = FormatStringToTextComponent(gameManager.score.ToString());
        }

        else if (m_MyText && gameManager.e_State == EGameState.STATE_GAMEOVER)
        {
            m_MyText.text = GameoverMessageStr(gameManager.score);
        }
    }

    private static string FormatStringToTextComponent(string str)
    {
        string text = str;
        char[] textArray = text.ToCharArray();
        string tempText = "";
        for (int i = 0; i < text.Length - 1; i++)
        {
            tempText += textArray[i] + " ";
        }
        return tempText;
    }

    public static string GameoverMessageStr(float score)
    {
        string scoreStr = "";
        if (score < 10)
            scoreStr = score.ToString();
        else
            scoreStr = FormatStringToTextComponent(score.ToString());
        return "Y O U   D I E D !\nS c o r e   " + scoreStr + "\nR e s t a r t i n g   g a m e . . .";
    }
}
