﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gridgenerator : MonoBehaviour {

	public gridTile tilePrefab;
    public GameObject wallPrefab;

	public int gridX;
	public int gridY;

	public int spacing;

	// Use this for initialization
	void Start () {
		CreateGrid ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void CreateGrid(){
		for (int i = 0; i < gridX; i++) {
			for (int j = 0; j < gridY; j++) {
				Vector3 position = new Vector3 (transform.position.x + spacing * i, 0, transform.position.z + spacing * j);
				gridTile tile = Instantiate (tilePrefab, position, Quaternion.identity, transform);
				tile.position = new Vector2 (i, j);
				if (j == 0 || i == 0 || j == gridY - 1 || i == gridX - 1) {
					tile.isBorder = true;

                    if (j == 0 || j == gridY - 1)
                    {
						if (i % 3 == 0) {
							Instantiate (wallPrefab, position, Quaternion.Euler (new Vector3 (0, 90, 0)));
						}
                    } else
                    {
						if (j % 3 == 0) {
							Instantiate (wallPrefab, position, Quaternion.Euler (new Vector3 (0, 0, 0)));
						}
                    }
                }
			}
		}
	}
}
