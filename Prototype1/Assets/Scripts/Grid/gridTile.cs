﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gridTile : MonoBehaviour
{

    public Vector2 position;
    public bool isTaken;
    public bool isBorder;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collision)
    {
		if (collision.gameObject.CompareTag("wall") || collision.gameObject.CompareTag("trap"))
        {
            isTaken = true;
        }

    }

    void OnTriggerExit(Collider collision)
    {
		if (collision.gameObject.CompareTag("wall") || collision.gameObject.CompareTag("wall"))
        {
            isTaken = false;
        }
    }
}
