﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectOnInput : MonoBehaviour
{
    public EventSystem eventSystem;
    public GameObject selectedObject;
    public GameObject previousSelectedObject;

    private bool buttonSelected;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Vertical") != 0 && buttonSelected == false && selectedObject.name.Contains("Dropdown"))
        {
            previousSelectedObject = selectedObject;
            Color normalTextColor = selectedObject.GetComponent<ReadButton>().normalColor;
            previousSelectedObject.GetComponentInChildren<Text>().color = normalTextColor;

            selectedObject = eventSystem.currentSelectedGameObject;
            Color hTextColor = selectedObject.GetComponent<ReadButton>().highlightedColor;
            selectedObject.GetComponentInChildren<Text>().color = hTextColor;
        }
    }

    private void OnDisable()
    {
        buttonSelected = false;
    }

    private void OnAwake()
    {
        eventSystem.SetSelectedGameObject(selectedObject);
    }

    private void OnEnable()
    {
        eventSystem.SetSelectedGameObject(selectedObject);
    }
}
