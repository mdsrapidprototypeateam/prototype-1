﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateScoreOnPanel : MonoBehaviour {

    ApplicationManager appManagerInst;
    public Text scoreText;
    public string scoreStr = "Scored ";
    public int iScore = 0;

	// Use this for initialization
	void Start () {
        appManagerInst = ApplicationManager.GetInstance();
        scoreText = GetComponent<Text>();
        scoreText.text = "";
    }

    // Update is called once per frame
    void Update () {
		if (iScore != appManagerInst.lastScore)
        {
            iScore = appManagerInst.lastScore;
            //scoreStr += iScore.ToString();
            scoreText.text = scoreStr + iScore.ToString();
        }
	}
}
