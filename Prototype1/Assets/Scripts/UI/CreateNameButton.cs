﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateNameButton : MonoBehaviour {

    public GameObject letterButtonPrefab;
    public int letterCount = 9;
    public string[] vecStrName;
    bool isInit = false;

    public Text saveButtonLabel;


    // Use this for initialization
    void Start () {
		if(letterButtonPrefab != null)
        {
            while(transform.childCount < letterCount)
            {
                Instantiate(letterButtonPrefab, transform);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (!isInit)
        {
            saveButtonLabel.text = "SAVE AS ANONYMOUS";
            for (int i = 0; i < letterCount; i++)
            {
                transform.GetChild(i).GetComponent<ChooseLetter>().nameCharIndex = i;
            }
            isInit = true;
        }
        for (int i = 0; i < letterCount; i++)
        {
            transform.GetChild(i).GetComponent<ChooseLetter>().letterText.text = vecStrName[i];
        }

        if (vecStrName[0] == "" && saveButtonLabel != null)
        {
            saveButtonLabel.text = "SAVE AS ANONYMOUS";
        } else if (vecStrName[0] != "" && saveButtonLabel != null)
        {
            saveButtonLabel.text = "SAVE";
        }
    }

    public void SavePlayerName()
    {
        ApplicationManager.GetInstance().playerName = "";
        string username = "";
        for (int i = 0; i < letterCount; i++)
        {
            if (vecStrName[i] != "")
                username += vecStrName[i];
        }
        ApplicationManager.UpdatePlayerName(username);

        ApplicationManager.GetInstance().PostPlayerScoreToDatabase();
    }
}
