﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LimitScroll : MonoBehaviour {

    RectTransform trf;

	// Use this for initialization
	void Start () {
        trf = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
		if (trf.localPosition.y <= 0)
        {
            Vector3 pos = trf.localPosition;
            pos.y = 0;
            trf.localPosition = pos;
        }
	}
}
