﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour
{
    public delegate void OnIntVariableChangeDelegate(int newVal);
    public event OnIntVariableChangeDelegate OnIntVariableChange;

    public delegate void OnStringVariableChangeDelegate(string newVal, Text textComponent);
    public event OnStringVariableChangeDelegate OnStringVariableChange;

    //public string username = "username";
    [SerializeField]
    private string m_Username = "username";
    [SerializeField]
    private string m_Score = "score";
    [SerializeField]
    private string m_Nplays = "nplays";
    [SerializeField]
    private string m_Ndwall = "ndwall";
    [SerializeField]
    private string m_Ndspykes = "ndspykes";
    [SerializeField]
    private string m_Ndlaser = "ndlaser";
    [SerializeField]
    private string m_Ndstopped = "ndstopped";
    [SerializeField]
    private string m_Nddepleted = "nddepleted";
    [SerializeField]
    private string m_Pos = "position";

    public string Pos
    {
        get { return m_Pos; }
        set
        {
            if (m_Pos == value) return;
            m_Pos = value;
            if (OnStringVariableChange != null)
                OnStringVariableChange(m_Pos, PosText);
        }
    }
    public string Username
    {
        get { return m_Username; }
        set
        {
            if (m_Username == value) return;
            m_Username = value;
            if (OnStringVariableChange != null)
                OnStringVariableChange(m_Username, UsernameText);
        }
    }
    public string Score
    {
        get { return m_Score; }
        set
        {
            if (m_Score == value) return;
            m_Score = value;
            if (OnStringVariableChange != null)
                OnStringVariableChange(m_Score, ScoreText);
        }
    }
    public string Nplays
    {
        get { return m_Nplays; }
        set
        {
            if (m_Nplays == value) return;
            m_Nplays = value;
            if (OnStringVariableChange != null)
                OnStringVariableChange(m_Nplays, NplaysText);
        }
    }
    public string Ndwall
    {
        get { return m_Ndwall; }
        set
        {
            if (m_Ndwall == value) return;
            m_Ndwall = value;
            if (OnStringVariableChange != null)
                OnStringVariableChange(m_Ndwall, NdwallText);
        }
    }
    public string Ndspykes
    {
        get { return m_Ndspykes; }
        set
        {
            if (m_Ndspykes == value) return;
            m_Ndspykes = value;
            if (OnStringVariableChange != null)
                OnStringVariableChange(m_Ndspykes, NdspykesText);
        }
    }
    public string Ndlaser
    {
        get { return m_Ndlaser; }
        set
        {
            if (m_Ndlaser == value) return;
            m_Ndlaser = value;
            if (OnStringVariableChange != null)
                OnStringVariableChange(m_Ndlaser, NdlaserText);
        }
    }
    public string Ndstopped
    {
        get { return m_Ndstopped; }
        set
        {
            if (m_Ndstopped == value) return;
            m_Ndstopped = value;
            if (OnStringVariableChange != null)
                OnStringVariableChange(m_Ndstopped, NdstoppedText);
        }
    }
    public string Nddepleted
    {
        get { return m_Nddepleted; }
        set
        {
            if (m_Nddepleted == value) return;
            m_Nddepleted = value;
            if (OnStringVariableChange != null)
                OnStringVariableChange(m_Nddepleted, NddepletedText);
        }
    }

    public Text PosText;
    public Text UsernameText;
    public Text ScoreText;
    public Text NplaysText;
    public Text NdwallText;
    public Text NdspykesText;
    public Text NdlaserText;
    public Text NdstoppedText;
    public Text NddepletedText;

    private void Start()
    {
        OnStringVariableChange += StringVariableChangeHandler;
        PosText = transform.GetChild(0).GetComponent<Text>();
        UsernameText = transform.GetChild(1).GetComponent<Text>();
        ScoreText = transform.GetChild(2).GetComponent<Text>();
        NplaysText = transform.GetChild(3).GetComponent<Text>();
        NdwallText = transform.GetChild(4).GetComponent<Text>();
        NdspykesText = transform.GetChild(5).GetComponent<Text>();
        NdlaserText = transform.GetChild(6).GetComponent<Text>();
        NdstoppedText = transform.GetChild(7).GetComponent<Text>();
        NddepletedText = transform.GetChild(8).GetComponent<Text>();

        Pos = "";
        Username = "";
        Score = "";
        Nplays = "";
        Ndwall = "";
        Ndspykes = "";
        Ndlaser = "";
        Ndstopped = "";
        Nddepleted = "";
    }

    private void StringVariableChangeHandler(string newVal, Text textComponent)
    {
        textComponent.text = newVal;
    }

    public void SetPosition(string value)
    {
        Pos = value;
    }

    public void SetUsername(string value)
    {
        Username = value;
    }
}
