﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChooseLetter : MonoBehaviour, ISelectHandler, IDeselectHandler
{

    public string abcStr = "abcdefghijklmnopqrstuvwxyz0123456789._ ";
    int abcStrLen = 0;
    public string strChar = "";
    public int currentChar = 0;
    public int previousChar = -1;
    public Text letterText;
    public int nameCharIndex = -1;
    public CreateNameButton parentNameComp;
    bool isInit = false;
    public bool isSelected = false;
    public bool isInvoking = false;

    public List<GameObject> arrows;
    private int arrowCount = 2;

    // Use this for initialization
    void Start()
    {
        Transform obj = transform.GetChild(0);
        for (int i = 0; i < obj.childCount; i++)
        {
            arrows.Add(obj.GetChild(i).gameObject);
        }
        arrowCount = arrows.Count;
        abcStr.ToUpper();
        abcStrLen = abcStr.Length;
        letterText = transform.GetComponentInChildren<Text>();
        parentNameComp = transform.GetComponentInParent<CreateNameButton>();

        currentChar = abcStr.Length -1;
        previousChar = currentChar - 1;

        //parentButton = GetComponentInParent<Button>();
    }

    // Update is called once per frame
    void Update () {
        if (!isInit)
        {
            letterText.text = "";
            isInit = true;
        }
        /*else if (nameCharIndex == 0 && !isInit)
        {
            currentChar = 0;
            previousChar = -1;
            letterText.text = strChar.ToUpper();
            isInit = true;
        }*/
        if (isSelected)
        {
            if (!isInvoking)
            {
                Invoke("CallChangeToCharIndex", 0.08f);
                isInvoking = true;
            }
        }
    }

    private void CallChangeToCharIndex()
    {
        float f = Mathf.Clamp(Input.GetAxis("Vertical"), -1.0f, 1.0f);
        ChangeToCharIndex(f);
        isInvoking = false;
    }

    public void ChangeToCharIndex(float i)
    {
        if (i > 0.2f)
        {
            if (currentChar == abcStrLen - 1)
            {
                currentChar = 0;
                previousChar = abcStrLen - 1;
            }
            else
            {
                previousChar = currentChar;
                currentChar++;
            }
        }
        else if(i < -0.2f)
        {
            if (currentChar == 0)
            {
                currentChar = abcStrLen - 1;
                previousChar = 0;
            }
            else {
                previousChar = currentChar;
                currentChar--;
            }
        }
        else if(i > -0.8f && i < 0.8f){

        }
        strChar = abcStr.Substring(currentChar, 1);
        //if (strChar == " ") strChar = "";
        letterText.text = strChar.ToUpper();

        parentNameComp.vecStrName[nameCharIndex] = strChar.ToUpper();
    }

    public void OnSelect(BaseEventData eventData)
    {
        //Debug.Log(currentChar + " is current char");
        isSelected = true;
        foreach (GameObject item in arrows)
        {
            item.SetActive(true);
        }
    }

    public void OnDeselect(BaseEventData eventData)
    {
        isSelected = false;
        foreach (GameObject item in arrows)
        {
            item.SetActive(false);
        }
    }
}
