﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DatabaseCalls : MonoBehaviour
{

    public JSONObject data = new JSONObject();

    public GameObject playerPrefab;

    public Text text;

    public List<JSONObject> userInfoList;
    public List<PlayerInfo> playerInfoList;

    private string _server = "http://brmatos.com/Flux/";
    private string _controller;
    private string _method;

    public int playerCount = 0;
    public bool isPopulated = false;

    public string _Url;

    string m_strUsernameLabel = "username";
    string m_strScoreLabel = "score";
    string m_strNplaysLabel = "nplays";
    string m_strDeathReasonLabel = "deathreason";
    string m_strDeathReasonCountLabel = "deathreasoncount";
    string m_strNdwallName = "ndwall";
    string m_strNdspykesName = "ndspykes";
    string m_strNdlaserName = "ndlaser";
    string m_strNdstoppedName = "ndstopped";
    string m_strNddepletedName = "nddepleted";

    public static string UsernameLabel = "username";
    public static string ScoreLabel = "score";
    public static string NplaysLabel = "nplays";
    public static string DeathReasonLabel = "deathreason";
    public static string DeathReasonCountLabel = "deathreasoncount";
    public static string NdwallName = "ndwall";
    public static string NdspykesName = "ndspykes";
    public static string NdlaserName = "ndlaser";
    public static string NdstoppedName = "ndstopped";
    public static string NddepletedName = "nddepleted";

    public bool isOnScoreBoard = false;

    // Use this for initialization
    void Start()
    {
        if (text)
            text.text = ""; //clean bottom text information
        userInfoList = new List<JSONObject>();
        playerInfoList = new List<PlayerInfo>();
    }

    public void SetOrderField(string orderField)
    {
        _controller = "index.php?";
        _method = "orderby=" + orderField;
    }

    public void SetOrderDir(string orderDirection)
    {
        _method += "&orderdir=" + orderDirection;
    }

    public void OrderByInt(int value)
    {
        switch (value)
        {
            case 0: //score
                SetOrderField(m_strScoreLabel);
                SetOrderDir("desc");
                break;
            case 1: //username
                SetOrderField(m_strUsernameLabel);
                SetOrderDir("asc");
                break;
            case 2: //nplays
                SetOrderField(m_strNplaysLabel);
                SetOrderDir("desc");
                break;
            case 3: //ndwall
                SetOrderField(m_strNdwallName);
                SetOrderDir("desc");
                break;
            case 4: //ndspykes
                SetOrderField(m_strNdspykesName);
                SetOrderDir("desc");
                break;
            case 5: //ndlaser
                SetOrderField(m_strNdlaserName);
                SetOrderDir("desc");
                break;
            case 6: //ndstopped
                SetOrderField(m_strNdstoppedName);
                SetOrderDir("desc");
                break;
            case 7: //nddepleted
                SetOrderField(m_strNddepletedName);
                SetOrderDir("desc");
                break;
        }

        ClearTable();
        RefreshTable();
    }

    // Update is called once per frame
    void Update()
    {
        if(isOnScoreBoard)
            PopulateTable();
    }

    void OnEnable()
    {
        //Instantiate (playerPrefab, transform);
        if(isOnScoreBoard)
            StartCoroutine(GetData());
    }

    void OnDisable()
    {
        if(isOnScoreBoard)
            ClearTable();
    }

    public IEnumerator GetData()
    {
        //Debug.Log ("it started");
        //_controller = "index.php?orderby=score";
        //_method = "";

        string url = _server + _controller + _method;
        _Url = url;
        //Debug.Log(url);
        WWW www = new WWW(url);

        //Debug.Log (www);
        yield return www;
        //Debug.Log (www);

        _controller = "";
        _method = "";

        if (www.bytesDownloaded <= 100)
        {
            yield return null;
            Debug.Log(" it is null");

        }
        else
        {
            if(isOnScoreBoard)
            {
                string responseOld = www.text;
                int index = responseOld.IndexOf(System.Environment.NewLine);
                responseOld = responseOld.Substring(index + System.Environment.NewLine.Length);
                string newResponse = responseOld.Substring(index + System.Environment.NewLine.Length);

                data = JSONObject.Create(newResponse);
                data.type = JSONObject.Type.ARRAY;
                playerCount = data.Count;

                for (int i = 0; i < playerCount; i++)
                {
                    GameObject obj = Instantiate(playerPrefab, transform);
                    PlayerInfo plInfo = obj.GetComponent<PlayerInfo>();
                    playerInfoList.Add(plInfo);
                }

                AccessData(data);
                text.text = "Total " + data.Count.ToString() + " players.";
            }
        }
    }

    void AccessData(JSONObject obj)
    {
        switch (obj.type)
        {
            case JSONObject.Type.OBJECT:
                for (int i = 0; i < obj.list.Count; i++)
                {
                    string key = (string)obj.keys[i];
                    JSONObject j = (JSONObject)obj.list[i];
                    Debug.Log(key);
                    AccessData(j);

                }
                break;
            case JSONObject.Type.ARRAY:
                /*foreach (JSONObject j in obj.list) {
                    //AccessData (j);
                    Debug.Log (j);
                }*/
                userInfoList = obj.list;
                break;
            case JSONObject.Type.STRING:
                Debug.Log(obj.str);


                break;
            case JSONObject.Type.BOOL:
                Debug.Log(obj.b);
                break;
            case JSONObject.Type.NUMBER:
                Debug.Log(obj.n);
                break;
            case JSONObject.Type.NULL:
                Debug.Log("Null");
                break;
        }

    }

    public void PopulateTable()
    {
        if(isOnScoreBoard)
        {
            if (playerInfoList.Count > 0 && !isPopulated)
            {
                for (int i = 0; i < playerCount; i++)
                {
                    string position = "" + (i + 1);
                    string username = userInfoList[i].GetField(m_strUsernameLabel).str;
                    string score = userInfoList[i].GetField(m_strScoreLabel).str;
                    string nplays = userInfoList[i].GetField(m_strNplaysLabel).str;
                    string ndwall = userInfoList[i].GetField(m_strNdwallName).str;
                    string ndspykes = userInfoList[i].GetField(m_strNdspykesName).str;
                    string ndlaser = userInfoList[i].GetField(m_strNdlaserName).str;
                    string ndstopped = userInfoList[i].GetField(m_strNdstoppedName).str;
                    string nddepleted = userInfoList[i].GetField(m_strNddepletedName).str;

                    playerInfoList[i].Pos = position;
                    playerInfoList[i].Username = username;
                    playerInfoList[i].Score = score;
                    playerInfoList[i].Nplays = nplays;
                    playerInfoList[i].Ndwall = ndwall;
                    playerInfoList[i].Ndspykes = ndspykes;
                    playerInfoList[i].Ndlaser = ndlaser;
                    playerInfoList[i].Ndstopped = ndstopped;
                    playerInfoList[i].Nddepleted = nddepleted;
                }
                isPopulated = true;
            }
        }
    }

    public void RefreshTable()
    {
        if(isOnScoreBoard)
            StartCoroutine(GetData());
    }

    public void ClearTable()
    {
        if(isOnScoreBoard)
        {
            for (int i = 1; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
            if (playerInfoList.Count > 0) playerInfoList.Clear();
            if (userInfoList.Count > 0) userInfoList.Clear();
            if (isPopulated) isPopulated = false;
        }
    }

    //insert.php?username=neo&score=5000&nplays=10&deathreason=nddepleted&deathreasoncount=1
    //insert.php?score=5000&nplays=10&deathreason=nddepleted&deathreasoncount=1
    //send request to post a new player entry or update an existing one (except for anonymous players)
    public IEnumerator PostPlayerScoreIEnum(string _playerName, int _score, int _nPlays, string _deathReasonLabel, int _deathReasonCount)
    {
        if(!isOnScoreBoard)
        {
            if (CheckForPlayer(_playerName))
                Debug.Log("Player not found in database.");

            Debug.Log("Posting something online.");

            _controller = "insert.php?";

            if (_playerName != "")
            {
                _method = m_strUsernameLabel + "=" + _playerName;
                _method += "&";
            }
            _method += m_strScoreLabel + "=" + _score.ToString();
            _method += "&" + m_strNplaysLabel + "=" + _nPlays.ToString();
            _method += "&" + m_strDeathReasonLabel + "=" + _deathReasonLabel;
            _method += "&" + m_strDeathReasonCountLabel + "=" + _deathReasonCount.ToString();


            string url = _server + _controller + _method;
            _Url = url;
            //Debug.Log(url);
            WWW www = new WWW(url);

            //Debug.Log (www);
            yield return www;
            //Debug.Log (www);

            _controller = "";
            _method = "";

            if (www.bytesDownloaded <= 100)
            {
                yield return null;
                Debug.Log(" it is null");

            }
        }
    }

    public void PostPlayerScore(string _playerName, int _score, int _nPlays, string _deathReasonLabel, int _deathReasonCount)
    {
        if(!isOnScoreBoard)
        {
            StartCoroutine(PostPlayerScoreIEnum(_playerName, _score, _nPlays, _deathReasonLabel, _deathReasonCount));
        }
    }


    //send request to check if already exists a player with the chosen name
    public bool CheckForPlayer(string _playerName)
    {
        return false;
    }
}
