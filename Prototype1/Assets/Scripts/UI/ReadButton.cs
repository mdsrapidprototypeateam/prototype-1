﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ReadButton : MonoBehaviour, ISelectHandler, IPointerEnterHandler, IPointerExitHandler, IDeselectHandler
{
    public Color highlightedColor;
    public Color normalColor;
    Text childTextComponent;

    private void Start()
    {
        childTextComponent = GetComponentInChildren<Text>();
        //normalColor = childTextComponent.color;
    }

    // When highlighted with mouse.
    public void OnPointerEnter(PointerEventData eventData)
    {
        // Do something.
        //Debug.Log("<color=green>Event:</color> Completed mouse highlight.");
        childTextComponent.color = highlightedColor;
        MenuAudio.PlayHoverSound();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("<color=red>Event:</color> Left item.");
        childTextComponent.color = normalColor;
    }

    // When selected.
    public void OnSelect(BaseEventData eventData)
    {
        // Do something.
        //Debug.Log("<color=red>Event:</color> Completed selection.");
        childTextComponent = GetComponentInChildren<Text>();
        childTextComponent.color = highlightedColor;
        MenuAudio.PlayHoverSound();
    }

    public void OnDeselect(BaseEventData eventData)
    {
        // Do something.
        //Debug.Log("<color=red>Event:</color> Completed deseselection.");
        childTextComponent = GetComponentInChildren<Text>();
        childTextComponent.color = normalColor;
    }
}
