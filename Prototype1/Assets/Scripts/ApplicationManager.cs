﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public enum EApplicationState
{
    NONE,
    STATE_MENU,
    STATE_LEVEL
}

public class ApplicationManager : MonoBehaviour {

    public EApplicationState e_ApplicationState = EApplicationState.NONE;
    string sceneName = "";

    public string playerName = "";
    public int lastScore;
    public int playCount;
    public int highestScore;
    public int playerScorePosition;
    public string lastDeathReason = "";
    public int lastDeathReasonCount;

    private DatabaseCalls m_DbCallsComponent;
    public MyGameManager m_GameManager;
    public static GameObject instance;
    public GameObject inGameMenuPanel;
    public GameObject MainMenuPanel;
    public GameObject creditsPanel;
    public GameObject TitlePanel;
    public GameObject uiCameraObj;

	[FMODUnity.EventRef]
	public string music = "event:/MainTheme";       //Create the eventref and define the event path
	public FMOD.Studio.EventInstance musicEvent;

    public static ApplicationManager GetInstance()
    {
        return GameObject.FindGameObjectWithTag("ApplicationManager").GetComponent<ApplicationManager>();
    }
    public void ToogleCredits()
    {
        creditsPanel.SetActive(!creditsPanel.activeSelf);
    }


    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //do stuff
        Debug.Log(scene.name);
		FMODUnity.RuntimeManager.GetBus ("bus:/").stopAllEvents (FMOD.Studio.STOP_MODE.IMMEDIATE);
        if (scene.name.Contains("Menu"))
        {
            uiCameraObj = GameObject.FindGameObjectWithTag("UICamera");
            uiCameraObj.SetActive(true);
            TitlePanel.SetActive(true);
            inGameMenuPanel.SetActive(false);
            e_ApplicationState = EApplicationState.STATE_MENU;
			musicEvent = FMODUnity.RuntimeManager.CreateInstance(music);
			musicEvent.start ();
			musicEvent.release ();
        }
        else
        {
            inGameMenuPanel.SetActive(false);
            MainMenuPanel.SetActive(false);
            uiCameraObj.SetActive(false);
            e_ApplicationState = EApplicationState.STATE_LEVEL;

            if (!m_GameManager)
            {
                m_GameManager = MyGameManager.GetInstance();
            }
        }
        sceneName = scene.name;
    }

// Use this for initialization
    void Start()
    {
        if(instance == null)
        {
            instance = gameObject;
            DontDestroyOnLoad(gameObject);
        }
        else if( instance != gameObject)
        {
            DestroyImmediate(gameObject);
            return;
        }

        if(!m_DbCallsComponent)
        {
            m_DbCallsComponent = GetComponent<DatabaseCalls>();
        }
    }

    public void ResetSceneName()
    {
        sceneName = "";
    }
	
	// Update is called once per frame
 	void Update () {
 		
 	}

    public static void UpdatePlayerName(string _playerName)
    {
        GetInstance().playerName = _playerName;
    }

    public static void UpdatePlayerLastScore(int _lastScore)
    {
        GetInstance().lastScore = _lastScore;
    }

    public static void UpdatePlayerPlayCount(int _playCount)
    {
         GetInstance().playCount = _playCount;
    }

    public static void UpdatePlayerHighestScore(int _highestScore)
    {
        GetInstance().highestScore = _highestScore;
    }

    public static void UpdatePlayerScorePosition(int _playerScorePosition)
    {
        GetInstance().playerScorePosition = _playerScorePosition;
    }

    public static void UpdatePlayerLastDeathReason(string _lastDeathReason)
    {
        GetInstance().lastDeathReason = _lastDeathReason;
    }

    public static void UpdatePlayerLastDeathReasonCountn(int _lastDeathReasonCount)
    {
        GetInstance().lastDeathReasonCount = _lastDeathReasonCount;
    }

    public static void UpdatePlayerInformation(
        string _playerName, int _lastScore, int _playCount,
        int _highestScore, int _playerScorePosition, string _lastDeathReason, int _lastDeathReasonCount)
    {
        GetInstance().playerName = _playerName;
        GetInstance().lastScore = _lastScore;
        GetInstance().playCount = _playCount;
        GetInstance().highestScore = _highestScore;
        GetInstance().playerScorePosition = _playerScorePosition;
        GetInstance().lastDeathReason = _lastDeathReason;
        GetInstance().lastDeathReasonCount = _lastDeathReasonCount;
    }

    public void PostPlayerScoreToDatabase()
    {
        m_DbCallsComponent.PostPlayerScore(playerName, lastScore, playCount, lastDeathReason, lastDeathReasonCount);
    }

    public void DisplayInGameMenu()
    {
        uiCameraObj.SetActive(true);
        inGameMenuPanel.SetActive(true);
    }
}
