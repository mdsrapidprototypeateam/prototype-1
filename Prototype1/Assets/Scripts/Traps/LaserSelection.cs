﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSelection : MonoBehaviour {

    public List<GameObject> lasers;
    
    public int numberOfLasers;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < numberOfLasers; i++)
        {
            lasers[i].SetActive(true);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
