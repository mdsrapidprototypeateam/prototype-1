﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerSound : MonoBehaviour {

    [FMODUnity.EventRef]
    public string spinning = "event:/SpinningBlade";       //Create the eventref and define the event path
    FMOD.Studio.EventInstance spinningEvent;

    // Use this for initialization
    void Start () {
        spinningEvent = FMODUnity.RuntimeManager.CreateInstance(spinning);
        spinningEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform));
        spinningEvent.start();
        spinningEvent.release();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
