﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserParent : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CallLaserActive()
    {
        transform.GetChild(0).GetComponent<LaserSetActive>().SetChildrenActive();
    }
}
