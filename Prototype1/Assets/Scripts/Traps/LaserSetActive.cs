﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSetActive : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetChildrenActive()
    {
        foreach(Transform child in transform)
        {
            LaserEmmiter em = child.GetComponent<LaserEmmiter>();
            if (em != null)
            {
                em.hasRaised = true;
            }
        }
    }
}
