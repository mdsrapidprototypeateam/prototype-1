﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserEmmiter : MonoBehaviour {

    public float cooldown;
    public float activeTime;
    float elapsedTime;
    GameObject laserObject;
    bool isActive;
    public bool hasRaised;
    bool blink;
	bool shouldBlink;

    [FMODUnity.EventRef]
    public string start = "event:/LaserStart";       //Create the eventref and define the event path
    public string keep = "event:/Laser";       //Create the eventref and define the event path
    public string end = "event:/LaserStop";       //Create the eventref and define the event path
    FMOD.Studio.EventInstance laserEvent;

    // Use this for initialization
    void Start () {
        laserObject = transform.GetChild(0).gameObject;
	}

    // Update is called once per frame
    void Update()
    {
        if (hasRaised)
        {
            if (blink)
            {
				if (shouldBlink) {
					laserObject.SetActive (!laserObject.activeInHierarchy);
					Color bColor = laserObject.GetComponent<Renderer> ().material.color;
					bColor = new Color (bColor.r, bColor.g, bColor.b, 0.1f);
				}

				shouldBlink = !shouldBlink;

            }
            if (isActive)
            {
                if (elapsedTime >= activeTime)
                {
                    laserObject.SetActive(false);
                    isActive = false;
                    elapsedTime = 0;
                    FMODUnity.RuntimeManager.PlayOneShot(end, transform.position);
                    laserEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                }
            }
            else
            {
                if (elapsedTime >= cooldown - 1f && elapsedTime < cooldown && !blink)
                {
                    blink = true;
                    laserObject.GetComponent<Collider>().enabled = false;
                    laserEvent = FMODUnity.RuntimeManager.CreateInstance(keep);
                    laserEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform));
                    laserEvent.start();
                    laserEvent.release();
                }
                if (elapsedTime >= cooldown)
                {
                    laserObject.SetActive(true);
                    laserObject.GetComponent<Collider>().enabled = true;
                    blink = false;
                    isActive = true;
                    elapsedTime = 0;
					Color bColor = laserObject.GetComponent<Renderer> ().material.color;
					bColor = new Color (bColor.r, bColor.g, bColor.b, 1f);
                }
            }

            elapsedTime += Time.deltaTime;
        }
    }
}
