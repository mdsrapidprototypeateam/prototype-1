﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAudio : MonoBehaviour {

    public static MenuAudio instance;

    [FMODUnity.EventRef]
    public string playSound = "event:/PlayGame";

    [FMODUnity.EventRef]
    public string hoverSound = "event:/Hover";

    [FMODUnity.EventRef]
    public string menuSound = "event:/OpenMenu";

    // Use this for initialization
    void Start () {
        instance = this;
	}
	
	// Update is called once per frame
	//void Update () {
		
	//}

    public void PlaySound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(playSound);
    }

    public void HoverSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(hoverSound);
    }

    public void MenuSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(menuSound);
    }

    public static void PlayEnterSceneSound()
    {
        instance.PlaySound();
    }
    public static void PlayHoverSound()
    {
        instance.HoverSound();
    }
    public static void PlayMenuSound()
    {
        instance.MenuSound();
    }
}
