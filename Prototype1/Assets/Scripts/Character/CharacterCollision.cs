﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCollision : MonoBehaviour {

    public MyGameManager gameManager;
    bool collidingWithWall;
    Rigidbody rb;

	void Start () {
		if ( !gameManager )
        {
            gameManager = (MyGameManager) GameObject.Find("GameManager").GetComponent<MyGameManager>();
        }

        rb = GetComponent<Rigidbody>();
    }
	
	void Update () {
        if (collidingWithWall)
        {
            if (rb.velocity.magnitude <= 0)
            {
                StopCoroutine("WallCollision");
                collidingWithWall = false;
            }
        }
	}

    void OnTriggerEnter(Collider collision)
    {
        if (gameManager.e_State == EGameState.STATE_GAME_STARTED)
        {
            //Debug.Log(collision.gameObject.tag);
            //Debug.Log(collision.gameObject.name);
			if (collision.gameObject.CompareTag ("trap")) {
				Debug.Log ("He dead bro");
				if (collision.gameObject.name.Contains ("Laser") || collision.gameObject.name.Contains ("Trail")) {
					gameManager.lastDeathReason = "Burnt by Laser";
					foreach (Transform t in GetComponent<Movement> ().ragdoll.transform) {
						t.gameObject.SetActive (false);
					}
					GetComponent<Movement> ().ragdoll.transform.Find ("Particle").gameObject.SetActive (true);
				}

				else if(collision.gameObject.name.Contains("spike"))
					gameManager.lastDeathReason = "Impaled by Spike Trap";

                else if (collision.gameObject.name.Contains("Blade") || collision.gameObject.name.Contains("Saw"))
                    gameManager.lastDeathReason = "Impaled by Spike Trap";
                gameManager.e_State = EGameState.STATE_GAMEOVER;
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (gameManager.e_State == EGameState.STATE_GAME_STARTED)
        {
            //Debug.Log(collision.gameObject.tag);
            //Debug.Log(collision.gameObject.name);
            if (collision.gameObject.CompareTag("wall"))
            {
                StartCoroutine("WallCollision");
            }
        }
    }

    IEnumerator WallCollision()
    {
        collidingWithWall = true;
        yield return new WaitForSeconds(0.5f);
		//Debug.Log("He dead bro");
		gameManager.lastDeathReason = "Crashed against wall";
        gameManager.e_State = EGameState.STATE_GAMEOVER;
    }
}
